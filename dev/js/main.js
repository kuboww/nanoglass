$(document).ready(function() {
    //custom scrollbar


    ;(function($){
            $(window).on("load",function(){
                $("#nav").mCustomScrollbar({
                    scrollbarPosition:'outside'

                });
            });
        })(jQuery);
//parallax
    $("section").mousemove(function(e) {
        var change;
        var xpos = e.clientX;
        var ypos = e.clientY;
        var left = change * 20;
        var xpos = xpos * 2;
        ypos = ypos * 2;

        $(this).find('.parallax__layer').css('top', ((0 + (ypos / 50)) + "px"));
        $(this).find('.parallax__layer').css('right', ((0 + (xpos / 80)) + "px"));

    });
    $("section").mouseleave(function(e) {
        $(this).find('.parallax__layer').css('top', '0');
        $(this).find('.parallax__layer').css('right', '0');
    });

// masonry
    // var $container = $('.masonry');
    // $container.imagesLoaded(function() {
    //     $container.masonry({
    //         itemSelector: '.masonry__item',
    //         columnWidth: '.masonry__item_sizer',
    //
    //     });
    // });
// equal height
    ;
    (function($, window, document, undefined) {
        'use strict';

        var $list = $('.eq'),
            $items = $list.find('.eq__item'),
            setHeights = function() {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function() {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);
        $list.find('img').on('load', setHeights);
    })(jQuery, window, document);


    //smoothscroll

     $(document).on("scroll", onScroll);


     $('#nav a[href*="#"]').on('click', function (e) {
         e.preventDefault();
         $(document).off("scroll");

         $('a').each(function () {
             $(this).removeClass('current');
         })
         $(this).addClass('current');

         var target = this.hash,
             menu = target;
         $target = $(target);
         $('html, body').stop().animate({
             'scrollTop': $target.offset().top
         }, 800, 'swing', function () {
             window.location.hash = target;
             $(document).on("scroll", onScroll);
         });
     });

     function onScroll(event){
         var scrollPos = $(document).scrollTop();
         $('#nav a[href*="#"]').each(function () {
             var currLink = $(this);
             var refElement = $(currLink.attr("href"));
             if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                 $('#nav a[href*="#"]').removeClass("current");
                 currLink.addClass("current");
             }
             else{
                 currLink.removeClass("current");
             }
         });
     }

     // phone mask
        //  $(function() {
        //      $('[name="phone"]').mask("+7(000)000-00-00", {
        //          clearIfNotMatch: true,
        //          placeholder: ""
        //      });
        //      $('[name="phone"]').focus(function(e) {
        //          if ($('[name="phone"]').val().length == 0) {
        //              $(this).val('+7(');
        //          }
        //      })
        //  });

        // floating placeholder
        //  $("input, textarea").each(function(e) {
        //           $(this).wrap('<fieldset></fieldset>');
        //           var tag = $(this).attr("placeholder");
        //           //var tag= $(this).data("tag");
        //           $(this).attr("placeholder", "");
        //           $(this).after('<label for="name">' + tag + '</label>');
        //         });

                $('input, textarea').on('blur', function() {
                  if (!$(this).val() == "") {
                    $(this).next().addClass('stay');
                  } else {
                    $(this).next().removeClass('stay');
                  }
                });



});
